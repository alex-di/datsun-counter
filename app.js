var express = require('express')
var app = express()
var gm = require('gm')
,   pw = require('promise.waterfall')

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/datsuncounter');
mongoose.Promise = Promise

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  let port = 8080
  app.listen(port, function () {
    console.log(`Example app listening on port ${port}!`)
  })
});

var pageSchema = mongoose.Schema({
    url: {type: String, index: true},
    count: {type: Number, default: 0}
});

pageSchema.methods.add = function(){
  this.count++
  this.save()
}

var Page = mongoose.model('Page', pageSchema)

app.set('view engine', 'pug')
app.use(express.static('public'))
app.get('/count', function(req, res){
  let { url } = req.query || req.headers.referer
  if(!url)
    return res.sendStatus(400)

  url = url.split('?')[0]
  Page.findOne({url})
  .then(page => {
    if(!page)
      page = new Page({url})

    page.add()
    res.json(page)
  })
})

app.get('/iframe', function(req, res){
  res.render('iframe')
})

app.get('/fixdoubles', function(req, res){
  Page.find({url: /\?/})
  .then(pages => {
    pw(pages.map(page => {
      return () => {

        let {url} = page
        url = url.split('?')[0]
        console.log('url', url)
        return Page.findOne({url})
        .then(base => {
          if(!base)
            base = new Page({url, count: 0})

          base.count += page.count
          page.remove()
          console.log(base)
          // return Promise.resolve()
          return base.save()

        })
        .catch(console.error)
      }
    }))
    .then(() => {
      res.json('ok')
      console.log('ok')
    })
    .catch(err => {
      console.log('error', err)
    })
  })
})


app.get('/image', function (req, res) {
  // x = 170
  // y = 60
  var url = req.headers.referer
  console.log(req)
  if(!url) return res.send(500)
  Page.findOne({url})
  .then(page => {
    if(!page)
      page = new Page({url})

    page.add()

    gm('./dummy.jpg')
    .font('sans-serif')
    .fill('#252669')
    .fontSize(50)
    .drawText(220, 92, page.count)
    // .write('./new.jpg', console.log)
    .toBuffer('JPG', function (err, buffer) {
    	if(err) console.log(err)
      res.setHeader("Expires", new Date().toUTCString());
      res.writeHead(200, {'Content-Type': 'image/jpeg'});
      res.write(buffer)
      res.end()
    })

  })
})

app.get('/test', function(req, res){
  res.writeHead(200, {'Content-Type': 'text/html'});
  res.end('<img src="/"/>')
})
